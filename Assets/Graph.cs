﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Graph : MonoBehaviour
{
    public List<GraphNode> Nodes;

    // Start is called before the first frame update

    void Awake()
    {
        Reset();
    }
    void Start()
    {
 
    }

    private void Reset()
    {
        Nodes = GetComponentsInChildren<GraphNode>(true).ToList();

        //Add two way connections for all nodes.
        foreach(GraphNode node in Nodes)
        {
            foreach(var conn in node.connections)
            {
                if (!conn.connections.Contains(node))
                {
                    conn.connections.Add(node);
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
