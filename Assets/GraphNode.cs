﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphNode : MonoBehaviour
{
    [SerializeField]
    public List<GraphNode> connections;

    [SerializeField]
    private string _id;

    [Range(1, 10)]
    public int cost;

    // Used for AStar
    [SerializeField]
    public float _gCost = 0;
    [SerializeField]
    public float _hCost = 0;
    [SerializeField]
    public float _fCost = 0;

    public GraphNode _parent;
    public int _index = -1;

    private void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, 0.8f);
        foreach (var node in connections)
        {
            Debug.DrawLine(node.transform.position, transform.position, Color.red);
        }
    }
}
