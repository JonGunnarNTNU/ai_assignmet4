﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    private float _numSec = 120;
    public Text _timerText;
    string _minutes;
    string _seconds;
    float _t;

    void Update()
    {
        // _timerText.text = Time.timeSinceLevelLoad.ToString();
        _minutes = ((int)Time.timeSinceLevelLoad / 60).ToString();
        _seconds = (Time.timeSinceLevelLoad % 60).ToString("f2");

        _timerText.text = _minutes + ":" + _seconds;
        if (Time.timeSinceLevelLoad >= _numSec)
        {
            Debug.Log("Game Done");
            Pause();
        }
    }

    public void Pause()
    {
        Debug.Log("Game Paused (not yet implemented)");

        // Time scale is normally 1, by setting it to 0,
        //  we can hopefully pause the game.
        Time.timeScale = 0;
    }

    // Reloads the scene, 
    // the idea is to make the game restart.
    public void ReloadScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}


