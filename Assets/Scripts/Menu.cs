﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Menu : MonoBehaviour
{
    // Load the scene
    public void PlayGame()
    {
        SceneManager.LoadScene("FlandersFields");
    }

}
