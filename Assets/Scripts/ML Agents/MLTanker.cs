﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

[RequireComponent(typeof(TankMovement))]
[RequireComponent(typeof(TankShooting))]
[RequireComponent(typeof(TankHealth))]
public class MLTanker : Agent
{
    private MLBootCamp myAcademy;
    public GameObject arena;
    private MLArena myArena;
    private TankMovement movement;
    private TankShooting shooting;
    public GameObject m_opponent;


    public bool contribute = false;

    public override void InitializeAgent()
    {
        base.InitializeAgent();
        movement = GetComponent<TankMovement>();
        shooting = GetComponent<TankShooting>();
        myAcademy = FindObjectOfType<MLBootCamp>();
        myArena = arena.GetComponent<MLArena>();
    }

    public override void CollectObservations()
    {
        base.CollectObservations();
        //add everything that the agent should keep track of:
        //positions and distance
        Vector3 ourPos = this.transform.position;
        Vector3 targetPos = m_opponent.transform.position;
        AddVectorObs(ourPos);
        AddVectorObs(targetPos);
        AddVectorObs(Vector3.Distance(ourPos, targetPos));
        //what way we are facing
        AddVectorObs(this.transform.rotation);
        //check line of sight to target(ignores layer 9 for players)
        bool rayHit = Physics.Linecast(ourPos, targetPos, ~(1 << 9));
        AddVectorObs(rayHit);
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {
        base.AgentAction(vectorAction, textAction);
        if (movement.Distance(transform.position, m_opponent.transform.position) > 20f)
        {
            Debug.Log(string.Format("{0} and {1}", vectorAction[0], (int)vectorAction[0]));
            movement.Firing(false);
            //if able to move node, do so
            //movement.MoveTo((int)vectorAction[0]);

            movement.Moveto_AStar(12 + 1);
        }
        else
        {
            movement.Firing(true);
            movement.snapTurn(m_opponent.transform.position);

            //if able to shoot, do so
            shooting.Fire();
        }
    }

    public override float[] Heuristic()
    {
        var action = new float[3];
        action[0] = Input.GetAxis("Horizontal1");
        action[1] = Input.GetAxis("Vertical1");
        action[2] = Input.GetAxis("Fire1");
        return action;
    }

    public override void AgentOnDone()
    {
        base.AgentOnDone();
    }

    public override void AgentReset()
    {
        base.AgentReset();
    }
}
