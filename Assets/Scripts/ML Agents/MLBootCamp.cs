﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class MLBootCamp : Academy
{
    private MLArena[] arenas;

    public override void AcademyReset()
    {
        if (arenas == null)
        {
            arenas = FindObjectsOfType<MLArena>();
        }
        foreach(var area in arenas)
        {
            area.ResetArea();
        }
    }

    private void Start()
    {
        AcademyReset();
    }
}
