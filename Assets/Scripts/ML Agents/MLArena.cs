﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAgents;

public class MLArena : Area
{
    public int numTanks = 2;
    public float areaRange = 50f; //size of playfield
    public List<MLTanker> tankers; //Container for tank agents
    public GameObject tankPrefab;

    // Destroyes old tanks and places new ones.
    public override void ResetArea()
    {
        foreach(var tank in tankers)
        {
            tank.AgentReset();
        }
    }

    private void CreateTanks()
    {
        var nTank = Instantiate(tankPrefab, new Vector3(25f, 5f, 25f), new Quaternion(), this.transform).GetComponent<MLTanker>();
        tankers.Add(nTank);

        var nTank2 = Instantiate(tankPrefab, new Vector3(-25f, 5f, -25f), new Quaternion(), this.transform).GetComponent<MLTanker>();
        tankers.Add(nTank2);

        nTank.m_opponent = nTank2.gameObject;
        nTank2.m_opponent = nTank.gameObject;
    }
}
