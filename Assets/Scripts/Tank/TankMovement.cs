﻿using UnityEngine;
using System.Collections.Generic;

public class TankMovement : MonoBehaviour
{
    public float m_Speed = 12f;
    public float m_TurnSpeed = 180f;
    public AudioSource m_MovementAudio;
    public AudioClip m_EngineIdling;
    public AudioClip m_EngineDriving;
    public float m_PitchRange = 0.2f;
    private Rigidbody m_Rigidbody;
    private Vector3 m_Previous_Direction;
    [SerializeField]
    private Vector3 m_Current_Pos;
    [SerializeField]
    private Vector3 m_Target_Pos;
    private Vector3 m_Current_dir;
    [SerializeField]
    public GameObject m_Graph;
    public GraphNode[] m_GraphNodes;
    public bool m_Firing = false;
    /*
    private string m_MovementAxisName;     
    private string m_TurnAxisName;                
    private float m_MovementInputValue;    
    private float m_TurnInputValue;        
    private float m_OriginalPitch;         


    private void OnEnable ()
    {
        m_Rigidbody.isKinematic = false;
        m_MovementInputValue = 0f;
        m_TurnInputValue = 0f;
    }


    private void OnDisable ()
    {
        m_Rigidbody.isKinematic = true;
    }


    private void Start()
    {
        m_MovementAxisName = "Vertical" + m_PlayerNumber;
        m_TurnAxisName = "Horizontal" + m_PlayerNumber;

        m_OriginalPitch = m_MovementAudio.pitch;
    }
    */

    void Start()
    {
        A_Star();
    }

    // ASTAR
    [SerializeField]
    public GraphNode _startNode;
    [SerializeField]
    public GraphNode _goalNode;

    [SerializeField]
    public List<GraphNode> _openList;
    [SerializeField]
    public List<GraphNode> _closedList;

    [SerializeField]
    public List<int> path = new List<int>();
    public int index = 0;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Current_Pos = m_Rigidbody.position;
        m_GraphNodes = m_Graph.GetComponentsInChildren<GraphNode>();

        _openList   = new List<GraphNode>();
        _closedList = new List<GraphNode>();
    }

    private void Update()
    {
        // Store the player's input and make sure the audio for the engine is playing.
    }


    private void EngineAudio()
    {
        // Play the correct audio clip based on whether or not the tank is moving and what audio is currently playing.
    }


    private void FixedUpdate()
    {

        if (!m_Firing)
        {
            // Move and turn the tank.
            if (index <= path.Count)
            {
                if (!Finished_Movement(transform.position, m_GraphNodes[path[(path.Count - 1) - index]].transform.position))
                {
                    MoveTo(path[(path.Count - 1) - index] + 1);
                }
                else
                {
                    index += 1;
                }
            }
            else
            {
                MoveTo(0);
            }
        }
        else
        {
            MoveTo(0);
        }
    }

    public void Firing(bool firing)
    {
        m_Firing = firing;
    }
    // Adjust the position of the tank
    public void MoveTo(Vector3 dest)
    {
        //Find the direction of movement
        Vector3 dir = (dest - m_Current_Pos).normalized;

        //Parameters might be the wrong way around
        Turn(m_Current_dir, dir);

        //Debug.Log("The function TankMovement.MoveTo(Vector3) is running but has not been tested");

        m_Current_Pos = m_Rigidbody.position;
        transform.position += (dir * Time.deltaTime * m_Speed);
    }

    public void Moveto_AStar(int targetNode)
    {
        if (_goalNode != m_GraphNodes[targetNode - 1])
        {
            //Set new target node
            SetGoalNode(m_GraphNodes[targetNode - 1]);

            //Clear the lists and reset our index
            path.Clear();
            _openList.Clear();
            _closedList.Clear();
            index = 0;

            //Run the A_star function
            A_Star();
        }
    }

    //Moves to the node in the graph given an index
    //The index should be +1, så the node in position[0] has index 1
    public void MoveTo(int index)
    {
        //Debug.Log("The function TankMovement.MoveTo(int) is running but has not been tested");

        if (index != 0)
            if (index <= m_GraphNodes.Length)
            {
                //Find the direction of movement
                Vector3 dir = (m_GraphNodes[index - 1].transform.position - m_Current_Pos).normalized;

                //Parameters might be the wrong way around
                snapTurn(m_GraphNodes[index - 1].transform.position);


                    m_Current_Pos = m_Rigidbody.position;
                transform.position += (dir * Time.deltaTime * m_Speed);
            }
    }

    //Returns wether or not the tank has reached it's destination
    //Use in tankmanager to know wether or not we have completed our orders.
    public bool Finished_Movement(Vector3 pos, Vector3 dest)
    {
        return (Distance(pos, dest) < 1.0f);
    }

    // Adjust the rotation of the tank based on a number of degrees
    public void Turn(float degrees)
    {
       // Debug.Log("The function TankMovement.Turn(float) is running but has not been tested");
        Quaternion turnRotation = Quaternion.Euler(0f, degrees * m_TurnSpeed * Time.deltaTime, 0f);
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }

    //Adjust the rotation of the tank based on it's position vs the position of it's target
    public void Turn(Vector3 X, Vector3 Y)
    {
        //Debug.Log("The function TankMovement.Turn(Vec3,Vec3) is running but has not been tested");
        float angle = Mathf.Atan2(Y.normalized.y, Y.normalized.x) - Mathf.Atan2(X.normalized.y, X.normalized.x);
        Quaternion turnRotation = Quaternion.Euler(0f, angle * m_TurnSpeed * Time.deltaTime, 0f);

        //Update Current Direction
        m_Current_dir.x = m_Current_dir.x * Mathf.Cos(angle * m_TurnSpeed * Time.deltaTime) - m_Current_dir.z * Mathf.Sin(angle * m_TurnSpeed * Time.deltaTime);
        m_Current_dir.z = m_Current_dir.x * Mathf.Sin(angle * m_TurnSpeed * Time.deltaTime) + m_Current_dir.z * Mathf.Cos(angle * m_TurnSpeed * Time.deltaTime);

        //Update Model
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * turnRotation);
    }

    //Adjust the Rotation of the tank based on an int value between 0 and 2
    public void Turn(int command)
    {
        float turn = 0;
        switch (command)
        {
            case 1:
                {
                    turn = 1;
                    break;
                }
            case 2:
                {
                    turn = -1;
                    break;
                }
            default: break;
        }
        Quaternion rotation = Quaternion.Euler(0f, turn * m_TurnSpeed * Time.deltaTime, 0f);
        m_Rigidbody.MoveRotation(m_Rigidbody.rotation * rotation);
    }

    //Snap to face a new rotation
    public void snapTurn(Vector3 target)
    {
        Vector3 dir = (target - m_Current_Pos).normalized;
        m_Rigidbody.rotation = Quaternion.LookRotation(dir, Vector3.up);
    }

    public float Distance(Vector3 x, Vector3 y)
    {
        return Mathf.Sqrt(Mathf.Pow((y.x - x.x), 2) + Mathf.Pow((y.y - x.y), 2) + Mathf.Pow((y.z - x.z), 2));
    }

    public void Shoot(Vector3 target)
    {
        Vector3 dir = (target - m_Current_Pos).normalized;

    }

    public void setTarget(Vector3 target)
    {
        m_Target_Pos = target;
    }

    public void A_Star(/*GraphNode start, GraphNode goal, float h*/)
    {
        // Sett the indexs for all nodes
        for (int i = 0; i < m_GraphNodes.Length; i++)
        {
            m_GraphNodes[i]._index = i;
        }

        // 1. Find GraphNode closest to this tank.
        GraphNode _closestNode = m_GraphNodes[0];
        float f_closest = Vector3.Distance(m_GraphNodes[0].transform.position, m_Current_Pos);
        int _closestIndex = 0;

        for (int i = 0; i < m_GraphNodes.Length; i++)
        {
            if (Vector3.Distance(m_GraphNodes[i].transform.position, m_Current_Pos) < f_closest)
            {
                f_closest = Vector3.Distance(m_GraphNodes[i].transform.position, m_Current_Pos);
                _closestNode = m_GraphNodes[i];
                _closestIndex = i;
            }
        }
        // with current postion of tank1, this will be BE
        _startNode = _closestNode;

        // 2.  Set _goalNode node
        //     for now since this is a testing example ill just use BY as the target

        // 3. Perform A*

        _openList.Add(_startNode);
        _startNode._gCost = 0;  // Starting node has gCost of 0
        _startNode._hCost = Vector3.Distance(_startNode.transform.position, _goalNode.transform.position);
        GraphNode _currentNode = _startNode; // Node with lowest fScore

        while(_openList.Count != 0)
        {
            if(_currentNode == _goalNode)
            {
               reconstruct_path(_currentNode);
            }

            _openList.Remove(_currentNode);
            _closedList.Add(_currentNode);

            foreach (var node in _currentNode.connections)
            {
                // Node is already checked
                if (_closedList.Contains(node))
                {
                    continue;
                }

                if (_openList.Contains(node))
                {
                    // check if new path is better
                    float newGCost = node._gCost + _currentNode._gCost;
                    if(newGCost < node._gCost)
                    {
                        // Path is better, update parent and cost.
                        node._gCost = newGCost;
                        node._parent = _currentNode;
                        node._fCost = node._gCost + node._hCost;
                        node._parent = _currentNode;
                    }
                }
                else
                {
                    _openList.Add(node);
                    node._gCost = _currentNode._gCost + Vector3.Distance(node.transform.position, _currentNode.transform.position);
                    node._hCost = Vector3.Distance(node.transform.position, _goalNode.transform.position); ;
                    node._fCost = node._gCost + node._hCost;
                    node._parent = _currentNode;
                }
            }

            if(_openList.Count != 0)
            {
                // Find the next node, i.e the one with the lowest f.cost
                _currentNode = _openList[0];
                for (int i = 0; i < _openList.Count; i++)
                {
                    if (_currentNode._fCost > _openList[i]._fCost)
                    {
                        _currentNode = _openList[i];
                    }
                }
            }
        }

        // This needs to be called before calling A* again
        // path.Clear();
        //_openList.Clear();
          //_closedList.Clear();
    }

    // function not yet done, based on the one from the wikipeida
    // article on ASTAR, ment to just make the path of nodes
    // after the target node is reached.
    public void reconstruct_path(GraphNode current)
    {
        //Debug.Log("FOUND A PATH!");
        //Debug.Log("PATH IS: ");

        int i = 1;

        // path.Add(_goalNode._index); dont think this is needed
        //                             since we know the target/goal node already.
        path.Add(current._index);
        while (current._parent != _startNode)
        {
            
          //  Debug.Log("PATH NODE NUMBER " + i + "is" + current._parent.name);
            current = current._parent;
            path.Add(current._index);
            i++;
        }
        //Debug.Log("Start node was: " + current._parent.name);
        path.Add(_startNode._index);

    }

    public void SetGoalNode(GraphNode newGoalNode)
    {
        _goalNode = newGoalNode;
    }

    public void SetStartNode(GraphNode newStartNode)
    {
        _startNode = newStartNode;
    }

    private void OnDrawGizmos()
    {
        foreach (var node in _startNode.connections)
        {
          //  Debug.Log("Start node is: " + node.name);
            //Debug.DrawLine(node.transform.position, transform.position, Color.green);
        }
    } 
}
 
 