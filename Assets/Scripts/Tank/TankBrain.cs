﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankBrain : MonoBehaviour
{
    [SerializeField]
    private Rigidbody _Rigidbody;

    // The graph nodes are way points,
    // but we should give them a radius
    // so the tank reach them more easy.
    [SerializeField]
    private GraphNode _targetNode;

    private TankMovement m_tankMovement;
    private TankShooting m_tankShooting; 
    
    private float _speed = 2.0f;


    // StateMachine
    public StateMachine _stateMachine;


    // Path variable that consists of a list of waypoints
    //                                     i.e graphNodes
    private void Awake()
    {
        _Rigidbody = GetComponent<Rigidbody>();
        m_tankMovement = GetComponent<TankMovement>();
        m_tankShooting = GetComponent<TankShooting>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    private void FixedUpdate()
    {
        // Move and turn the tank.
        m_tankMovement.Turn(1);
        m_tankMovement.MoveTo(new Vector3(0f, 0f, 0f));
        //m_tankShooting.Fire(_Rigidbody);
    }

    void MoveToTarget()
    {
        // Move
        transform.position = Vector3.MoveTowards(transform.position,
                                _targetNode.transform.position, Time.deltaTime * _speed);

        Turn();
    }

    public void Turn()
    {
        /*
        THIS CODE ROTATES SLOWLY BUT DOEST NOT STOP XD
        float degrees = 5.0f;
        float _TurnSpeed = 1.0f;
        // Rotate
        Quaternion turnRotation = Quaternion.Euler(0f, degrees * _TurnSpeed * Time.deltaTime, 0f);
        _Rigidbody.MoveRotation(_Rigidbody.rotation * turnRotation);*/

        // Instant Rotate -_-
        var targetDistance = _targetNode.gameObject.transform.position - transform.position;
        var targetDirection = targetDistance;

        targetDirection.Normalize();
        this.transform.rotation = Quaternion.LookRotation(targetDirection, Vector3.up);
    }


    void SetTargetNode(GraphNode i_node)
    {
        _targetNode = i_node;
    }

    void SetPath()
    {

    }


    void Attack()
    {

    }
}
