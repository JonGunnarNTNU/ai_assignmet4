﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{  
    public Rigidbody m_Shell;
    public float m_Launchforce;
    public Transform m_FireTransorm;

    private float timer;
    /*
    private string m_FireButton;         
    private float m_CurrentLaunchForce;  
    private float m_ChargeSpeed;         
    private bool m_Fired;                


    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
        m_FireButton = "Fire" + m_PlayerNumber;

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }
    */

    private void Update()
    {
        // Track the current state of the fire button and make decisions based on the current launch force.
    }

    private void FixedUpdate()
    {
        timer += Time.deltaTime;   
    }

    public void Fire()
    {
        if (timer > 1.0f)
        {
            timer = 0.0f;
            // Instantiate and launch the shell.
            Rigidbody i_Shell =Instantiate(m_Shell,m_FireTransorm,false) as Rigidbody;
            i_Shell.velocity = m_Launchforce * m_FireTransorm.forward;
        }
    }
}